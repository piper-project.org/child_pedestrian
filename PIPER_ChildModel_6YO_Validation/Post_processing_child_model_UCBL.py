# -*- coding: utf-8 -*-
# This file runs all cfile files with LS-Prepost that could find in the subdirectories, outputs are processed 
# and are plotted along experimental curves.
# Arguments: none. You just have to put the path of LSPP and to enable (post_treat_LSPP=1) or disable (post_treat_LSPP=0)
# to specify if binout have to be processed
# Author: PDB
# License: Creative Commons Attribution 4.0 International License.
# (https://creativecommons.org/licenses/by/4.0/)
#
# This work has received funding from the European Union Seventh Framework 
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# Contributor: Jeremie Peres
#
import numpy as np
import matplotlib.pyplot as plt
import os
import re

post_treat_LSPP=1 #set 1 if binout have to be processed 

numeric_const_pattern = r""" [-+]? (?: (?: \d* \. \d+ ) | (?: \d+ \.? ) )  (?: [Ee] [+-]? \d+ ) ? """
rx = re.compile(numeric_const_pattern, re.VERBOSE)
  

# lsPath is a string with the location of Ls-Prepost on the computer
lsPath='C:\\Users\\pdb\\Desktop\\LS-PrePost_4.5\\lsprepost4.5_x64.exe '
# resPath is a string with the location of the result folder "PIPER_Child_6YO_Validation" on the computer
#resPath=os.path.join(os.getcwd())+'\\'
resPath='C:\\Users\\pdb\\Desktop\\PIPER_Child_model_versions\\PIPER_child_pedestrian_v1_0_1\\'
Path = lsPath+resPath

###############################################################################
###################### >>6 y-0 child<< ########################################
###############################################################################

############################Shoulder###########################################

# EEVC Shoulder 6yo
path_temp='PIPER_ChildModel_6YO_Validation\\Shoulder\\EEVC-Q2008\\'
file=open(resPath+path_temp+'pp_EEVC-Q2008.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "binout0000"\n')
file.write('binaski fileswitch binout0000;\n')
file.write('binaski loadblock /rcforc\n')
file.write('binaski plot "binout0000" rcforc 1 1 S-1 y_force ;\n')
file.write('xyplot 1 savefile xypair "force" 1 all\n')
file.write('binaski loadblock /nodout\n')
file.write('genselect node add node 4116934/0\n')
file.write('binaski plot "binout0000" nodout 1 1 4116934 y_displacement ;\n')
file.write('xyplot 1 savefile xypair "nodout1" 1 all\n')
file.write('genselect node add node 20001709/0\n')
file.write('binaski plot "binout0000" nodout 1 1 20001709 y_displacement ;\n')
file.write('xyplot 1 savefile xypair "nodout2" 1 all\n')
file.write('quit\n')
file.close()

if post_treat_LSPP:
    os.system(Path+path_temp+'pp_EEVC-Q2008.cfile -nographics')
force_sim_EEVC_shoul_6y0=np.loadtxt(resPath+path_temp+'force',skiprows=1)
nod1_EEVC_shoul_6y0=np.loadtxt(resPath+path_temp+'nodout1',skiprows=1)
nod2_EEVC_shoul_6y0=np.loadtxt(resPath+path_temp+'nodout2',skiprows=1)

############################Thorax#############################################

# Eppinger 2006 Thorax 6yo low energy
path_temp='PIPER_ChildModel_6YO_Validation\\Thorax\\Eppinger_2006_low\\'
file=open(resPath+path_temp+'pp_Eppinger_2006_low.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "binout0000"\n')
file.write('binaski fileswitch binout0000;\n')
file.write('binaski loadblock /rcforc\n')
file.write('binaski plot "binout0000" rcforc 1 1 S-1 y_force ;\n')
file.write('xyplot 1 savefile xypair "force" 1 all\n')
file.write('quit\n')
file.close()

if post_treat_LSPP:
    os.system(Path+path_temp+'pp_Eppinger_2006_low.cfile -nographics')
force_sim_Epp_low_Thorax_6y0=np.loadtxt(resPath+path_temp+'force',skiprows=1)

# Eppinger 2006 Thorax 6yo high energy
path_temp='PIPER_ChildModel_6YO_Validation\\Thorax\\Eppinger_2006_high\\'
file=open(resPath+path_temp+'pp_Eppinger_2006_high.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "binout0000"\n')
file.write('binaski fileswitch binout0000;\n')
file.write('binaski loadblock /rcforc\n')
file.write('binaski plot "binout0000" rcforc 1 1 S-1 y_force ;\n')
file.write('xyplot 1 savefile xypair "force" 1 all\n')
file.write('quit\n')
file.close()

if post_treat_LSPP:
    os.system(Path+path_temp+'pp_Eppinger_2006_high.cfile -nographics')
force_sim_Epp_high_Thorax_6y0=np.loadtxt(resPath+path_temp+'force',skiprows=1)

############################Neck###############################################

# NBDL Lateral 7g Neck 6yo
path_temp='PIPER_ChildModel_6YO_Validation\\Neck\\NBDL_lat_7g\\'
file=open(resPath+path_temp+'pp_NBDL_7g.cfile','w')
file.write('binaski init;\n')
file.write('binaski load "binout0000"\n')
file.write('binaski fileswitch binout0000;\n')
file.write('binaski loadblock /nodout\n')
file.write('genselect node add node 1150000/0\n')
file.write('binaski plot "binout0000" nodout 1 1 1150000 rx_acceleration ;\n')
file.write('xyplot 1 filter sae 180.0 msec 0\n')
file.write('xyplot 1 savefile xypair "rx_acc" 1 all\n')
file.write('binaski plot "binout0000" nodout 1 1 1150000 ry_acceleration ;\n')
file.write('xyplot 1 filter sae 180.0 msec 0\n')
file.write('xyplot 1 savefile xypair "ry_acc" 1 all\n')
file.write('binaski plot "binout0000" nodout 1 1 1150000 rz_acceleration ;\n')
file.write('xyplot 1 filter sae 180.0 msec 0\n')
file.write('xyplot 1 savefile xypair "rz_acc" 1 all\n')
file.write('binaski plot "binout0000" nodout 1 1 1150000 x_acceleration ;\n')
file.write('xyplot 1 filter sae 180.0 msec 0\n')
file.write('xyplot 1 savefile xypair "x_acc" 1 all\n')
file.write('binaski plot "binout0000" nodout 1 1 1150000 y_acceleration ;\n')
file.write('xyplot 1 filter sae 180.0 msec 0\n')
file.write('xyplot 1 savefile xypair "y_acc" 1 all\n')
file.write('binaski plot "binout0000" nodout 1 1 1150000 z_acceleration ;\n')
file.write('xyplot 1 filter sae 180.0 msec 0\n')
file.write('xyplot 1 savefile xypair "z_acc" 1 all\n')
file.write('binaski plot "binout0000" nodout 1 1 1150000 rx_displacement ;\n')
file.write('xyplot 1 filter sae 180.0 msec 0\n')
file.write('xyplot 1 savefile xypair "rx_rot" 1 all\n')
file.write('binaski plot "binout0000" nodout 1 1 1150000 ry_displacement ;\n')
file.write('xyplot 1 filter sae 180.0 msec 0\n')
file.write('xyplot 1 savefile xypair "ry_rot" 1 all\n')
file.write('binaski plot "binout0000" nodout 1 1 1150000 rz_displacement ;\n')
file.write('xyplot 1 filter sae 180.0 msec 0\n')
file.write('xyplot 1 savefile xypair "rz_rot" 1 all\n')
file.write('binaski plot "binout0000" nodout 1 1 1150003 x_displacement ;\n')
file.write('xyplot 1 filter sae 180.0 msec 0\n')
file.write('xyplot 1 savefile xypair "x_disp" 1 all\n')
file.write('binaski plot "binout0000" nodout 1 1 1150003 y_displacement ;\n')
file.write('xyplot 1 filter sae 180.0 msec 0\n')
file.write('xyplot 1 savefile xypair "y_disp" 1 all\n')
file.write('binaski plot "binout0000" nodout 1 1 1150003 z_displacement ;\n')
file.write('xyplot 1 filter sae 180.0 msec 0\n')
file.write('xyplot 1 savefile xypair "z_disp" 1 all\n')
file.write('binaski loadblock /rbdout\n')
file.write('binaski plot "binout0000" rbdout 1 1 global 2116 global_dy ;\n')
file.write('xyplot 1 filter sae 180.0 msec 0\n')
file.write('xyplot 1 savefile xypair "y_disp2" 1 all\n')
file.write('quit\n')
file.close()

if post_treat_LSPP:
    os.system(Path+path_temp+'pp_NBDL_7g.cfile -nographics')
x_acc_sim_NBDL_lat_7g_Piper_6y0=np.loadtxt(resPath+path_temp+'x_acc',skiprows=1)
y_acc_sim_NBDL_lat_7g_Piper_6y0=np.loadtxt(resPath+path_temp+'y_acc',skiprows=1)
z_acc_sim_NBDL_lat_7g_Piper_6y0=np.loadtxt(resPath+path_temp+'z_acc',skiprows=1)
x_disp_sim_NBDL_lat_7g_Piper_6y0=np.loadtxt(resPath+path_temp+'x_disp',skiprows=1)
y_disp_sim_NBDL_lat_7g_Piper_6y0=np.loadtxt(resPath+path_temp+'y_disp',skiprows=1)
z_disp_sim_NBDL_lat_7g_Piper_6y0=np.loadtxt(resPath+path_temp+'z_disp',skiprows=1)
rx_acc_sim_NBDL_lat_7g_Piper_6y0=np.loadtxt(resPath+path_temp+'rx_acc',skiprows=1)
ry_acc_sim_NBDL_lat_7g_Piper_6y0=np.loadtxt(resPath+path_temp+'ry_acc',skiprows=1)
rz_acc_sim_NBDL_lat_7g_Piper_6y0=np.loadtxt(resPath+path_temp+'rz_acc',skiprows=1)
rx_rot_sim_NBDL_lat_7g_Piper_6y0=np.loadtxt(resPath+path_temp+'rx_rot',skiprows=1)
ry_rot_sim_NBDL_lat_7g_Piper_6y0=np.loadtxt(resPath+path_temp+'ry_rot',skiprows=1)
rz_rot_sim_NBDL_lat_7g_Piper_6y0=np.loadtxt(resPath+path_temp+'rz_rot',skiprows=1)
y_disp2_sim_NBDL_lat_7g_Piper_6y0=np.loadtxt(resPath+path_temp+'y_disp2',skiprows=1)
x_acc_exp_NBDL_lat_7g_Piper_6y0_inf = np.loadtxt(resPath+path_temp+'Lateral_7g_x_lin_acc_mm_vs_ms2_low.txt')
x_acc_exp_NBDL_lat_7g_Piper_6y0_sup = np.loadtxt(resPath+path_temp+'Lateral_7g_x_lin_acc_mm_vs_ms2_high.txt')
y_acc_exp_NBDL_lat_7g_Piper_6y0_inf = np.loadtxt(resPath+path_temp+'Lateral_7g_y_lin_acc_mm_vs_ms2_low.txt')
y_acc_exp_NBDL_lat_7g_Piper_6y0_sup = np.loadtxt(resPath+path_temp+'Lateral_7g_y_lin_acc_mm_vs_ms2_high.txt')
z_acc_exp_NBDL_lat_7g_Piper_6y0_inf = np.loadtxt(resPath+path_temp+'Lateral_7g_z_lin_acc_mm_vs_ms2_low.txt')
z_acc_exp_NBDL_lat_7g_Piper_6y0_sup = np.loadtxt(resPath+path_temp+'Lateral_7g_z_lin_acc_mm_vs_ms2_high.txt')
x_disp_exp_NBDL_lat_7g_Piper_6y0_inf = np.loadtxt(resPath+path_temp+'Lateral_7g_x_dis_mm_vs_ms_low.txt')
x_disp_exp_NBDL_lat_7g_Piper_6y0_sup = np.loadtxt(resPath+path_temp+'Lateral_7g_x_dis_mm_vs_ms_high.txt')
y_disp_exp_NBDL_lat_7g_Piper_6y0_inf = np.loadtxt(resPath+path_temp+'Lateral_7g_y_dis_mm_vs_ms_low.txt')
y_disp_exp_NBDL_lat_7g_Piper_6y0_sup = np.loadtxt(resPath+path_temp+'Lateral_7g_y_dis_mm_vs_ms_high.txt')
z_disp_exp_NBDL_lat_7g_Piper_6y0_inf = np.loadtxt(resPath+path_temp+'Lateral_7g_z_dis_mm_vs_ms_low.txt')
z_disp_exp_NBDL_lat_7g_Piper_6y0_sup = np.loadtxt(resPath+path_temp+'Lateral_7g_z_dis_mm_vs_ms_high.txt')
x_rot_exp_NBDL_lat_7g_Piper_6y0_inf = np.loadtxt(resPath+path_temp+'Lateral_7g_x_rot_deg_vs_ms_low.txt')
x_rot_exp_NBDL_lat_7g_Piper_6y0_sup = np.loadtxt(resPath+path_temp+'Lateral_7g_x_rot_deg_vs_ms_high.txt')
y_rot_exp_NBDL_lat_7g_Piper_6y0_inf = np.loadtxt(resPath+path_temp+'Lateral_7g_y_rot_deg_vs_ms_low.txt')
y_rot_exp_NBDL_lat_7g_Piper_6y0_sup = np.loadtxt(resPath+path_temp+'Lateral_7g_y_rot_deg_vs_ms_high.txt')
z_rot_exp_NBDL_lat_7g_Piper_6y0_inf = np.loadtxt(resPath+path_temp+'Lateral_7g_z_rot_deg_vs_ms_low.txt')
z_rot_exp_NBDL_lat_7g_Piper_6y0_sup = np.loadtxt(resPath+path_temp+'Lateral_7g_z_rot_deg_vs_ms_high.txt')
x_rot_acc_exp_NBDL_lat_7g_Piper_6y0_inf = np.loadtxt(resPath+path_temp+'Lateral_7g_x_ang_acc_rad_vs_ms2_low.txt')
x_rot_acc_exp_NBDL_lat_7g_Piper_6y0_sup = np.loadtxt(resPath+path_temp+'Lateral_7g_x_ang_acc_rad_vs_ms2_high.txt')
y_rot_acc_exp_NBDL_lat_7g_Piper_6y0_inf = np.loadtxt(resPath+path_temp+'Lateral_7g_y_ang_acc_rad_vs_ms2_low.txt')
y_rot_acc_exp_NBDL_lat_7g_Piper_6y0_sup = np.loadtxt(resPath+path_temp+'Lateral_7g_y_ang_acc_rad_vs_ms2_high.txt')
z_rot_acc_exp_NBDL_lat_7g_Piper_6y0_inf = np.loadtxt(resPath+path_temp+'Lateral_7g_z_ang_acc_rad_vs_ms2_low.txt')
z_rot_acc_exp_NBDL_lat_7g_Piper_6y0_sup = np.loadtxt(resPath+path_temp+'Lateral_7g_z_ang_acc_rad_vs_ms2_high.txt')

###############################################################################
########################Plots results##########################################
###############################################################################

###################### >>6 y-0 child<< ########################################

f = plt.figure(figsize=(28,20))
ax1 = f.add_subplot(441)
ax1.set_title('Shoulder lateral impact, EEVC-2008')
# Shoulder lateral, force
corridorH_x=(0,4,16,35)
corridorH_y=(0.5,0.8,0.8,0.3)
corridorL_x=(0,8,26)
corridorL_y=(0,0.5,0.2)
ax1.plot(force_sim_EEVC_shoul_6y0[:,0], force_sim_EEVC_shoul_6y0[:,1], 'r', label='PIPER_child_model')
ax1.plot(corridorH_x, corridorH_y, 'k--', label='Irwin_2002_corridor')
ax1.plot(corridorL_x, corridorL_y, 'k--')
ax1.legend(fontsize=8, loc='best')
ax1.set_xlabel('time (ms)')
ax1.set_ylabel('force (kN)')

ax2 = f.add_subplot(442)
ax2.set_title('Shoulder lateral impact, EEVC-2008')
# Shoulder lateral, deflexion
corridorH_x=(0,40)
corridorH_y=(25,25)
corridorL_x=(0,40)
corridorL_y=(21,21)
ax2.plot(nod1_EEVC_shoul_6y0[:,0],nod1_EEVC_shoul_6y0[:,1]-nod2_EEVC_shoul_6y0[:,1], 'r', label='PIPER_child_model')
ax2.plot(corridorH_x, corridorH_y, 'k--', label='Irwin_2002_corridor')
ax2.plot(corridorL_x, corridorL_y, 'k--')
ax2.legend(fontsize=8, loc='best')
ax2.set_xlabel('time (ms)')
ax2.set_ylabel('deflexion (mm)')

ax3 = f.add_subplot(443)
ax3.set_title('Thorax lat. LE, Eppinger 2006')
# Thorax lateral low energy, force
corridorH_x=(0.0,6.0,9.0,28.0)
corridorH_y=(0.5,1.1,1.1,0.6)
corridorL_x=(0.0,6.0,19.0,25.0)
corridorL_y=(0.0,0.5,0.5,0.0)
ax3.plot(force_sim_Epp_low_Thorax_6y0[:,0], force_sim_Epp_low_Thorax_6y0[:,1], 'r', label='PIPER_child_model')
ax3.plot(corridorH_x, corridorH_y, 'k--', label='Irwin_2002_corridor')
ax3.plot(corridorL_x, corridorL_y, 'k--')
ax3.legend(fontsize=8, loc='best')
ax3.set_xlabel('time (ms)')
ax3.set_ylabel('force (kN)')

ax4 = f.add_subplot(444)
ax4.set_title('Thorax lat. HE, Eppinger 2006')
# Thorax lateral high energy, force
corridorH_x=(0.0,3.0,16.0,28.0)
corridorH_y=(0.3,1.4,1.4,0.7)
corridorL_x=(0.0,9.0,16.0,28.0)
corridorL_y=(0.0,0.8,0.8,0.0)
ax4.plot(force_sim_Epp_high_Thorax_6y0[:,0], force_sim_Epp_high_Thorax_6y0[:,1], 'r', label='PIPER_child_model')
ax4.plot(corridorH_x, corridorH_y, 'k--', label='Irwin_2002_corridor')
ax4.plot(corridorL_x, corridorL_y, 'k--')
ax4.legend(fontsize=8, loc='best')
ax4.set_xlabel('time (ms)')
ax4.set_ylabel('force (kN)')

ax5 = f.add_subplot(445)
ax5.set_title('Neck lat.7g, NBDL, x-acc')
# Neck lateral, x acceleration
ax5.plot(x_acc_sim_NBDL_lat_7g_Piper_6y0[:,0], -1000*x_acc_sim_NBDL_lat_7g_Piper_6y0[:,1], 'r', label='PIPER_child_model')
ax5.plot(x_acc_exp_NBDL_lat_7g_Piper_6y0_inf[:,0],x_acc_exp_NBDL_lat_7g_Piper_6y0_inf[:,1],'--k', label='Wismans_corridor')
ax5.plot(x_acc_exp_NBDL_lat_7g_Piper_6y0_sup[:,0],x_acc_exp_NBDL_lat_7g_Piper_6y0_sup[:,1],'--k')
ax5.legend(fontsize=8, loc='best')
ax5.set_xlabel('time (ms)')
ax5.set_ylabel('Acceleration (m/s2)')

ax6 = f.add_subplot(446)
ax6.set_title('Neck lat.7g, NBDL, y-acc')
# Neck lateral, y acceleration
ax6.plot(y_acc_sim_NBDL_lat_7g_Piper_6y0[:,0], -1000*y_acc_sim_NBDL_lat_7g_Piper_6y0[:,1], 'r', label='PIPER_child_model')
ax6.plot(y_acc_exp_NBDL_lat_7g_Piper_6y0_inf[:,0],y_acc_exp_NBDL_lat_7g_Piper_6y0_inf[:,1],'--k', label='Wismans_corridor')
ax6.plot(y_acc_exp_NBDL_lat_7g_Piper_6y0_sup[:,0],y_acc_exp_NBDL_lat_7g_Piper_6y0_sup[:,1],'--k')
ax6.legend(fontsize=8, loc='best')
ax6.set_xlabel('time (ms)')
ax6.set_ylabel('Acceleration (m/s2)')

ax7 = f.add_subplot(447)
ax7.set_title('Neck lat.7g, NBDL, z-acc')
# Neck lateral, z acceleration
ax7.plot(z_acc_sim_NBDL_lat_7g_Piper_6y0[:,0], -1000*z_acc_sim_NBDL_lat_7g_Piper_6y0[:,1], 'r', label='PIPER_child_model')
ax7.plot(z_acc_exp_NBDL_lat_7g_Piper_6y0_inf[:,0],z_acc_exp_NBDL_lat_7g_Piper_6y0_inf[:,1],'--k', label='Wismans_corridor')
ax7.plot(z_acc_exp_NBDL_lat_7g_Piper_6y0_sup[:,0],z_acc_exp_NBDL_lat_7g_Piper_6y0_sup[:,1],'--k')
ax7.legend(fontsize=8, loc='best')
ax7.set_xlabel('time (ms)')
ax7.set_ylabel('Acceleration (m/s2)')

ax8 = f.add_subplot(448)
ax8.set_title('Neck lat.7g, NBDL, x-disp')
# Neck lateral, x displacement
ax8.plot(x_disp_sim_NBDL_lat_7g_Piper_6y0[:,0], -x_disp_sim_NBDL_lat_7g_Piper_6y0[:,1], 'r', label='PIPER_child_model')
ax8.plot(x_disp_exp_NBDL_lat_7g_Piper_6y0_inf[:,0],x_disp_exp_NBDL_lat_7g_Piper_6y0_inf[:,1],'--k', label='Wismans_corridor')
ax8.plot(x_disp_exp_NBDL_lat_7g_Piper_6y0_sup[:,0],x_disp_exp_NBDL_lat_7g_Piper_6y0_sup[:,1],'--k')
ax8.legend(fontsize=8, loc='best')
ax8.set_xlabel('time (ms)')
ax8.set_ylabel('Displacement (mm)')

ax9 = f.add_subplot(449)
ax9.set_title('Neck lat.7g, NBDL, y-disp')
# Neck lateral, y displacement
ax9.plot(y_disp_sim_NBDL_lat_7g_Piper_6y0[:,0], -y_disp_sim_NBDL_lat_7g_Piper_6y0[:,1]+y_disp2_sim_NBDL_lat_7g_Piper_6y0[:,1], 'r', label='PIPER_child_model')
ax9.plot(y_disp_exp_NBDL_lat_7g_Piper_6y0_inf[:,0],y_disp_exp_NBDL_lat_7g_Piper_6y0_inf[:,1],'--k', label='Wismans_corridor')
ax9.plot(y_disp_exp_NBDL_lat_7g_Piper_6y0_sup[:,0],y_disp_exp_NBDL_lat_7g_Piper_6y0_sup[:,1],'--k')
ax9.legend(fontsize=8, loc='best')
ax9.set_xlabel('time (ms)')
ax9.set_ylabel('Displacement (mm)')

ax10 = f.add_subplot(4,4,10)
ax10.set_title('Neck lat.7g, NBDL, z-disp')
# Neck lateral, z displacement
ax10.plot(z_disp_sim_NBDL_lat_7g_Piper_6y0[:,0], z_disp_sim_NBDL_lat_7g_Piper_6y0[:,1], 'r', label='PIPER_child_model')
ax10.plot(z_disp_exp_NBDL_lat_7g_Piper_6y0_inf[:,0],z_disp_exp_NBDL_lat_7g_Piper_6y0_inf[:,1],'--k', label='Wismans_corridor')
ax10.plot(z_disp_exp_NBDL_lat_7g_Piper_6y0_sup[:,0],z_disp_exp_NBDL_lat_7g_Piper_6y0_sup[:,1],'--k')
ax10.legend(fontsize=8, loc='best')
ax10.set_xlabel('time (ms)')
ax10.set_ylabel('Displacement (mm)')

ax11 = f.add_subplot(4,4,11)
ax11.set_title('Neck lat.7g, NBDL, rx-acc')
# Neck lateral, x rotational acceleration
ax11.plot(rx_acc_sim_NBDL_lat_7g_Piper_6y0[:,0], -1e+6*rx_acc_sim_NBDL_lat_7g_Piper_6y0[:,1], 'r', label='PIPER_child_model')
ax11.plot(x_rot_acc_exp_NBDL_lat_7g_Piper_6y0_inf[:,0],x_rot_acc_exp_NBDL_lat_7g_Piper_6y0_inf[:,1],'--k', label='Wismans_corridor')
ax11.plot(x_rot_acc_exp_NBDL_lat_7g_Piper_6y0_sup[:,0],x_rot_acc_exp_NBDL_lat_7g_Piper_6y0_sup[:,1],'--k')
ax11.legend(fontsize=8, loc='best')
ax11.set_xlabel('time (ms)')
ax11.set_ylabel('Rotational Acceleration (rad/s2)')

ax12 = f.add_subplot(4,4,12)
ax12.set_title('Neck lat.7g, NBDL, ry-acc')
# Neck lateral, x rotational acceleration
ax12.plot(ry_acc_sim_NBDL_lat_7g_Piper_6y0[:,0], 1e+6*ry_acc_sim_NBDL_lat_7g_Piper_6y0[:,1], 'r', label='PIPER_child_model')
ax12.plot(y_rot_acc_exp_NBDL_lat_7g_Piper_6y0_inf[:,0],y_rot_acc_exp_NBDL_lat_7g_Piper_6y0_inf[:,1],'--k', label='Wismans_corridor')
ax12.plot(y_rot_acc_exp_NBDL_lat_7g_Piper_6y0_sup[:,0],y_rot_acc_exp_NBDL_lat_7g_Piper_6y0_sup[:,1],'--k')
ax12.legend(fontsize=8, loc='best')
ax12.set_xlabel('time (ms)')
ax12.set_ylabel('Rotational Acceleration (rad/s2)')

ax13 = f.add_subplot(4,4,13)
ax13.set_title('Neck lat.7g, NBDL, rz-acc')
# Neck lateral, x rotational acceleration
ax13.plot(rz_acc_sim_NBDL_lat_7g_Piper_6y0[:,0], -1e+6*rz_acc_sim_NBDL_lat_7g_Piper_6y0[:,1], 'r', label='PIPER_child_model')
ax13.plot(z_rot_acc_exp_NBDL_lat_7g_Piper_6y0_inf[:,0],z_rot_acc_exp_NBDL_lat_7g_Piper_6y0_inf[:,1],'--k', label='Wismans_corridor')
ax13.plot(z_rot_acc_exp_NBDL_lat_7g_Piper_6y0_sup[:,0],z_rot_acc_exp_NBDL_lat_7g_Piper_6y0_sup[:,1],'--k')
ax13.legend(fontsize=8, loc='best')
ax13.set_xlabel('time (ms)')
ax13.set_ylabel('Rotational Acceleration (rad/s2)')

ax14 = f.add_subplot(4,4,14)
ax14.set_title('Neck lat.7g, NBDL, rx-rot')
# Neck lateral, x rotational displacement
ax14.plot(rx_rot_sim_NBDL_lat_7g_Piper_6y0[:,0], -57.32*rx_rot_sim_NBDL_lat_7g_Piper_6y0[:,1], 'r', label='PIPER_child_model')
ax14.plot(x_rot_exp_NBDL_lat_7g_Piper_6y0_inf[:,0],x_rot_exp_NBDL_lat_7g_Piper_6y0_inf[:,1],'--k', label='Wismans_corridor')
ax14.plot(x_rot_exp_NBDL_lat_7g_Piper_6y0_sup[:,0],x_rot_exp_NBDL_lat_7g_Piper_6y0_sup[:,1],'--k')
ax14.legend(fontsize=8, loc='best')
ax14.set_xlabel('time (ms)')
ax14.set_ylabel('Rotation (°)')

ax15 = f.add_subplot(4,4,15)
ax15.set_title('Neck lat.7g, NBDL, ry-rot')
# Neck lateral, x rotational displacement
ax15.plot(ry_rot_sim_NBDL_lat_7g_Piper_6y0[:,0], -7.5+57.32*ry_rot_sim_NBDL_lat_7g_Piper_6y0[:,1], 'r', label='PIPER_child_model')
ax15.plot(y_rot_exp_NBDL_lat_7g_Piper_6y0_inf[:,0],y_rot_exp_NBDL_lat_7g_Piper_6y0_inf[:,1],'--k', label='Wismans_corridor')
ax15.plot(y_rot_exp_NBDL_lat_7g_Piper_6y0_sup[:,0],y_rot_exp_NBDL_lat_7g_Piper_6y0_sup[:,1],'--k')
ax15.legend(fontsize=8, loc='best')
ax15.set_xlabel('time (ms)')
ax15.set_ylabel('Rotation (°)')

ax16 = f.add_subplot(4,4,16)
ax16.set_title('Neck lat.7g, NBDL, rz-rot')
# Neck lateral, x rotational displacement
ax16.plot(rz_rot_sim_NBDL_lat_7g_Piper_6y0[:,0], 2.5-57.32*rz_rot_sim_NBDL_lat_7g_Piper_6y0[:,1], 'r', label='PIPER_child_model')
ax16.plot(z_rot_exp_NBDL_lat_7g_Piper_6y0_inf[:,0],z_rot_exp_NBDL_lat_7g_Piper_6y0_inf[:,1],'--k', label='Wismans_corridor')
ax16.plot(z_rot_exp_NBDL_lat_7g_Piper_6y0_sup[:,0],z_rot_exp_NBDL_lat_7g_Piper_6y0_sup[:,1],'--k')
ax16.legend(fontsize=8, loc='best')
ax16.set_xlabel('time (ms)')
ax16.set_ylabel('Rotation (°)')

plt.tight_layout()
plt.show()
f.savefig(resPath+'PIPER_ChildModel_6YO_Validation\\Results_6yo_pedestrian.png')