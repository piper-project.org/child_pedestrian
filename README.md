This repository contains the PIPER Child Pedestrian Human Body Model (HBM) and its 
validation setups.

The PIPER Child Pedestrian HBM is released under the GNU General Public License GPL v3
with additional liability and open science clauses.

The validation setups are released under a Creative Commons attribution license
(CC-BY-4.0) to encourage reproduction with other models.

Please see the respective folders for the details and the list of contributors.


This work has received funding from the European Union Seventh Framework 
Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).